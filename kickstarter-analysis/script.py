import pandas as pd
from matplotlib import pyplot as plt

# Kickstarter exports all reward tiers as separate CSV files
# reward1 = pd.read_csv('5.00 NZD Receive Updates - Feb 14 08pm.csv')
# reward2 = pd.read_csv('15.00 NZD Thank You + Receive Updates - Feb 14 08pm.csv')
# reward3 = pd.read_csv('25.00 NZD 1_20th Investment in a Squawk Squad - Feb 14 08pm.csv')
# reward4 = pd.read_csv('50.00 NZD 1_10th Investment in a Squawk Squad - Feb 14 08pm.csv')
# reward5 = pd.read_csv('60.00 NZD Exclusive Squawk Squad Shirt - Feb 14 08pm.csv')
# reward6 = pd.read_csv('80.00 NZD 1_20th Trap + Exclusive SS Shirt - Feb 14 08pm.csv')
# reward7 = pd.read_csv('100.00 NZD 1_5th Investment in a Squawk Squad - Feb 14 08pm.csv')
# reward8 = pd.read_csv('220.00 NZD 1_2 Investment in a Squawk Squad - Feb 14 08pm.csv')
# reward9 = pd.read_csv('270.00 NZD 1_2 Investment + Exclusive SS Shirt - Feb 14 08pm.csv')
# reward10 = pd.read_csv('290.00 NZD 1_2 Trap + Birds of NZ Book - Feb 14 08pm.csv')
# reward11 = pd.read_csv('400.00 NZD Full Trap Investment - Feb 14 08pm.csv')
# reward12 = pd.read_csv('460.00 NZD Full Trap + Birds of NZ Book - Feb 14 08pm.csv')
# reward13 = pd.read_csv('800.00 NZD 1 Full Trap + 2x Tiritiri Matangi Tour - Feb 14 08pm.csv')
# reward14 = pd.read_csv('2_200.00 NZD 2 Traps + 2x Trip to Milford Sounds - Feb 14 08pm.csv')
# no_reward = pd.read_csv('No reward - Feb 14 08pm.csv')

# Merge all CSV files into one using outer join for union
# combine = reward1.merge(reward2, how='outer').merge(reward3, how='outer').merge(reward4, how='outer')\
#     .merge(reward5, how='outer').merge(reward5, how='outer').merge(reward6, how='outer')\
#     .merge(reward7, how='outer').merge(reward8, how='outer').merge(reward9, how='outer')\
#     .merge(reward10, how='outer').merge(reward11, how='outer').merge(reward12, how='outer')\
#     .merge(reward13, how='outer').merge(reward14, how='outer').merge(no_reward, how='outer')

# Clean DataFrame by dropping columns containing any personal or unnecessary information
# print(combine.columns)
# combine = combine.drop(['Backer UID', 'Backer Name', 'Email',
#        'Rewards Sent?', 'Pledged Status', 'Notes',
#        'Survey Response', 'Reward ID', 'Billing State/Province',
#        'What Is The Email Address Of The Person/Group You Would Like To Receive Notifications?',
#        'What Would You Like Your Squawk Squad Username To Be?',
#        'Shipping Country', 'Shipping Amount', 'Shipping Name',
#        'Shipping Address 1', 'Shipping Address 2', 'Shipping City',
#        'Shipping State', 'Shipping Postal Code', 'Shipping Country Name',
#        'Shipping Country Code', 'Shipping Phone Number',
#        'Shipping Delivery Notes',
#        'What Size Would You Like Your Squawk Squad T Shirt To Be?',
#        'What Is The Email Address Of The Person(S)/Group You Would Like To Receive Notifications?',
#        'What Would You Like Your Squawk Squad Username(S) To Be?'], axis=1)

# Make all data anonymous to preserve customer confidentiality
# combine.to_csv('kickstarter.csv', encoding='utf-8')

# create kickstarter DataFrame
kickstarter = pd.read_csv('kickstarter.csv')

# Basic checks that the number of backers matches the number of unique backer ID's
print('Total number of backers:', len(kickstarter))
print('Final backer ID number:', max(kickstarter['Backer Number']))
sorted_list = sorted(kickstarter['Backer Number'])

# people who backed the campaign but did not follow through with their pledge
def backers_who_dropped(backer_ids):
    all_backers = [i for i in range(backer_ids[0], backer_ids[-1] + 1)]
    failed_backer_id = set(backer_ids)
    return list(failed_backer_id ^ set(all_backers))


dropped_ids = backers_who_dropped(sorted_list)
print('\nBacker ID\'s from those who initially supported the campaign but dropped out before making a payment:'\
      , dropped_ids)
print('Percentage of people who dropped out before payment was taken:', "%.2f%%" % ((len(dropped_ids)/len(kickstarter))*100))

unique_rewards = kickstarter['Reward Title'].nunique()
mode_reward = kickstarter['Reward Title'].mode()

def tier_price(row):
        if row['Reward Title'] == 'Receive Updates':
            return 5
        elif row['Reward Title'] == 'Thank You + Receive Updates':
            return 15
        elif row['Reward Title'] == '1/20th Investment in a Squawk Squad':
            return 25
        elif row['Reward Title'] == '1/10th Investment in a Squawk Squad':
            return 50
        elif row['Reward Title'] == 'Exclusive Squawk Squad Shirt':
            return 60
        elif row['Reward Title'] == '1/20th Trap + Exclusive SS Shirt':
            return 80
        elif row['Reward Title'] == '1/5th Investment in a Squawk Squad':
            return 100
        elif row['Reward Title'] == '1/2 Investment in a Squawk Squad':
            return 220
        elif row['Reward Title'] == '1/2 Investment + Exclusive SS Shirt':
            return 270
        elif row['Reward Title'] == '1/2 Trap + Birds of NZ Book':
            return 290
        elif row['Reward Title'] == 'Full Trap Investment':
            return 400
        elif row['Reward Title'] == 'Full Trap + Birds of NZ Book':
            return 460
        elif row['Reward Title'] == '1 Full Trap + 2x Tiritiri Matangi Tour':
            return 800
        elif row['Reward Title'] == '2 Traps + 2x Trip to Milford Sounds':
            return 2200
        else:
            return 0


kickstarter['Price Tier'] = kickstarter.apply(lambda row: tier_price(row), axis=1)

# Plot bar chart of reward tiers and number of people supporting
fig, ax = plt.subplots(figsize=(15,7))
kickstarter.groupby(['Price Tier']).count()['Backer Number'].plot(ax=ax, kind='bar', color='black')
ax.set_title('Total Kickstarter Backers By Price Tier')
ax.set_xlabel('Price Tiers for Backer Rewards')
ax.set_ylabel('Number of Backers')
plt.show()

total_raised = 70256
total_backers = kickstarter['Backer Number'].count()
AOV = total_raised/total_backers
print('Average order value: NZD$', round(AOV, 2))
print('\nThere are', unique_rewards, 'unique reward tiers.')
mode_reward = kickstarter['Price Tier'].mode()[0]
print('The most popular reward price tier was: NZD$', mode_reward)

# The most popular reward was $25, but how much did it contribute overall?
most_popular_tier = len(kickstarter[kickstarter['Price Tier'] == 25])
most_popular_pct = ((most_popular_tier*25)/total_raised)*100
print('With', most_popular_tier, '(', round((most_popular_tier/total_backers)*100, 2), '%)',\
      'backers this was by far the most popular pricing tier however it ',\
      'only raised ', round(most_popular_pct, 2), '% of the total amount pledged.')

# only 2% of backer supported at the $460 tier and raised a similar percent towards the total amount pledged
tier_460 = len(kickstarter[kickstarter['Price Tier'] == 460])
tier_460_pct = ((tier_460*460)/total_raised)*100
print('While only', tier_460,'(', round((tier_460/total_backers)*100,2), '%) of backers raised nearly the same amount',\
      'with', round(tier_460_pct, 2), '% of the total amount pledged.')
tier_spread = kickstarter['Price Tier'].std()
print('There is a large spread in our tier pledges as shown by the standard deviation:', round(tier_spread, 2))

# Looks at the country our backers are pledging from
total_countries = kickstarter['Billing Country'].nunique()
country_codes = sorted(['New Zealand', 'United States', 'Canada', 'Great Britain', 'Australia', 'Singapore',\
                        'Netherlands', 'Germany', 'France', 'China', 'Denmark', 'Sweeden', 'Finland',\
                        'Arab Emirates', 'South Africa', 'Japan', 'Thailand'])
print('\nWe have pledges from',total_countries,'countries around the world.')
fig, ax = plt.subplots(figsize=(15,7))
kickstarter.groupby(['Billing Country']).count()['Backer Number'].plot(ax=ax, kind='bar', color='black')
ax.set_title('Number of Backers by Country')
ax.set_xlabel('Country Codes')
ax.set_xticklabels(country_codes, rotation=30)
ax.set_ylabel('Number of Backers')
plt.show()

# Based on the plot above, NZ is the country with the largest support. Get that value as a percentage
nz_supporters = len(kickstarter[kickstarter['Billing Country'] == 'NZ'])
print('While 17 countries supported the campaign, New Zealanders funded a majority with:', "%.2f%%" %\
      ((nz_supporters/total_backers)*100))

# Stretch goal votes for each sanctuary. Note: not all backers submitted a vote
penguin = len(kickstarter[kickstarter['Which Sanctuary Would You Like To See Our Second Project Take Place?']\
            == 'Te Rere Penguin Reserve, The Catlins (project aimed at protecting the Yellow-eyed penguin/hoiho)'])
kiwi = len(kickstarter[kickstarter['Which Sanctuary Would You Like To See Our Second Project Take Place?']\
            == 'Thames Coast Kiwi Care, Coromandel Region (project aimed at protecting the brown kiwi)'])
kokako = len(kickstarter[kickstarter['Which Sanctuary Would You Like To See Our Second Project Take Place?']\
            == 'Pukaha Mt Bruce, Wellington Region (project aimed at protecting the kōkako)'])

votes = [penguin, kiwi, kokako]
sanctuary_names = ['Te Rere Penguin Reserve', 'Thames Coast Kiwi Care', 'Pukaha Mt Bruce']
plt.pie(votes, autopct='%0.1f%%')
plt.legend(sanctuary_names)
plt.title('Backer Votes for Stretch Goal of Second Sanctuary Choice')
plt.axis('equal')
plt.show()
