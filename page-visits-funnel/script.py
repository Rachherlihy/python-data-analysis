import pandas as pd

visits = pd.read_csv('visits.csv',
                     parse_dates=[1])
cart = pd.read_csv('cart.csv',
                   parse_dates=[1])
checkout = pd.read_csv('checkout.csv',
                       parse_dates=[1])
purchase = pd.read_csv('purchase.csv',
                       parse_dates=[1])

# percentage of visitors who did not put anything in their cart
first_step = pd.merge(visits, cart, how='left')
first_len = len(first_step)
null_ = first_step[first_step.cart_time.isnull()]
null_len = len(null_)
prct = float(null_len)/float(first_len)
print('\nPercentage of users who visit the store but do not place anything in their cart: ' + '%.2f%%' % (prct*100))

# percentage of visitors who put things into their cart but do not progress to checkout
second_step = pd.merge(cart, checkout, how='left')
sec_len = len(second_step)
null_2 = second_step[second_step.checkout_time.isnull()]
null_2_len = len(null_2)
prct_2 = float(null_2_len)/float(sec_len)
print('\nPercent of users who put items in cart but did not checkout: ' + '%.2f%%' % (prct_2*100))

# merge all data to contain all steps of the funnel
all_data = visits.merge(cart, how='left').merge(checkout, how='left').merge(purchase, how='left')
print('\nNew dataframe consists of columns with values:')
print(list(all_data))

# number of visitors who reached checkout but drop off before purchasing
checkout_drop = all_data[(all_data.purchase_time.isnull()) & (all_data.checkout_time.notnull())]
drop_total = len(checkout_drop)
user_total = len(all_data)
drop_prct = float(drop_total)/float(user_total)
print('\nPercentage of visitors who reached checkout but did not make a purchase: ' + '%.2f%%' % (drop_prct*100))

# calculates the average time it takes for a customer to make a purchase
all_data['time_to_purchase'] = all_data.purchase_time - all_data.visit_time
time_formatted = str(all_data.time_to_purchase.mean()).replace(':', ' hours and ', 1).split('.', 2)[0] + str(' minutes.')
print('The average time to purchase:', time_formatted)