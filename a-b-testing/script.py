import pandas as pd

ad_clicks = pd.read_csv('ad_clicks.csv')
# see first ten rows of ad_clicks
print(ad_clicks.head(10))

# see which ad platform is getting the most views
utm_count = ad_clicks.groupby(['utm_source']).user_id.count().reset_index()

# if timestamp is not null then user clicked on an ad
ad_clicks['is_click'] = ~ad_clicks.ad_click_timestamp.isnull()

# percent of people who clicked on ads from each utm source
clicks_by_source = ad_clicks.groupby(['utm_source', 'is_click']).user_id.count().reset_index()

# display as a pivot table
clicks_pivot = clicks_by_source.pivot(
  columns='is_click',
  index='utm_source',
  values='user_id'
).reset_index()

# find percent of users who clicked on the ad from each utm_source
clicks_pivot['percent_clicked'] = clicks_pivot[True] / (clicks_pivot[True] + clicks_pivot[False])

# check to see if the same number of people were shown both ads
groups = ad_clicks.groupby(['experimental_group']).user_id.count()
print('\n'*2, groups)

# check to see if a greater percent of users click on ad A or B
compare_groups = ad_clicks.groupby(['experimental_group', 'is_click']).user_id.count().reset_index()
# show this as a pivot table
compare_group_pivot = compare_groups.pivot(
  columns='is_click',
  index='experimental_group',
  values='user_id'
).reset_index()
print('\n'*2, compare_group_pivot)

# create separate DataFrames for groups A and B
a_clicks = ad_clicks[ad_clicks.experimental_group == 'A']
b_clicks = ad_clicks[ad_clicks.experimental_group == 'B']

a_percent_clicked = a_clicks[a_clicks.is_click == True]
b_percent_clicked = b_clicks[b_clicks.is_click == True]
# calculates percent of clicks by users per day
a_by_day = a_percent_clicked.groupby(['day']).user_id.count().reset_index()
b_by_day = b_percent_clicked.groupby(['day']).user_id.count().reset_index()

# could print separate by using above a_by_day or b_by_day or could show in one DataFrame with below
both_groups = ad_clicks[ad_clicks.is_click == True].groupby(['experimental_group', 'day']).user_id.count().reset_index()
print('\n'*2, both_groups)

print('\n'*2, both_groups.groupby(['day', 'experimental_group', ]).user_id.max())
